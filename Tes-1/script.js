const btnAdd = document.querySelector("button");
const mhs = [{
    nama: "John",
    umur: 26,
    jurusan: "Informatika",
    alamat: "Jakarta",
}, ];

btnAdd.addEventListener("click", () => {
    const table = document.querySelector("table");
    const body = table.createTBody();
    const td1 = document.createElement("td");
    const td2 = document.createElement("td");
    const td3 = document.createElement("td");
    const td4 = document.createElement("td");

    mhs.map((mahasiswa) => {
        td1.innerHTML = mahasiswa.nama;
        td2.innerHTML = mahasiswa.umur;
        td3.innerHTML = mahasiswa.jurusan;
        td4.innerHTML = mahasiswa.alamat;

        body.append(td1, td2, td3, td4);
    });
});

// navbar-----------------------
const body = document.querySelector("body");
const nav = document.createElement("nav");
const ul = document.createElement("ul");
const li1 = document.createElement("li");
const li2 = document.createElement("li");
const li3 = document.createElement("li");

const a1 = document.createElement("a");
const a2 = document.createElement("a");
const a3 = document.createElement("a");

a1.setAttribute("href", "index.html");
a2.setAttribute("href", "contact.html");
a3.setAttribute("href", "#");

a1.innerText = "Beranda";
a2.innerText = "Kontak";
a3.innerText = "Mahasiswa";

li1.append(a1);
li2.append(a2);
li3.append(a3);

li1.append();
nav.append(ul);
ul.append(li1, li2, li3);

nav.style.height = "50px";
nav.style.backgroundColor = "white";
nav.style.display = "flex";
nav.style.justifyContent = "right";
nav.style.alignItems = "center";
nav.style.padding = "0px 200px";
nav.style.boxShadow = "0 0 5px rgba(0,0,0,0.5)";
nav.style.position = "sticky";
nav.style.top = "0";

ul.style.display = "flex";
ul.style.gap = "20px";
li1.style.listStyle = "none";
li2.style.listStyle = "none";
li3.style.listStyle = "none";

a1.style.textDecoration = "none";
a2.style.textDecoration = "none";
a3.style.textDecoration = "none";

a1.style.color = "#acacac";
a2.style.color = "#acacac";
a3.style.color = "#acacac";

body.prepend(nav);