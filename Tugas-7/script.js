const body = document.querySelector("body");
body.style.fontFamily = "arial";

const text = document.createElement("p");
text.innerHTML =
    "Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut commodi voluptatibus temporibus sequi impedit possimus aspernatur exercitationem debitis voluptas cum qui corrupti nemo quae libero, suscipit perferendis ad inventore velit?";

const img = document.createElement("img");
img.setAttribute("src", "img/tenor.jpg");
img.style.width = "100px";
img.style.height = "100px";

const h3 = document.createElement("h3");
h3.innerText = "Our Partners";

const ul = document.createElement("ul");
const li1 = document.createElement("li");
const li2 = document.createElement("li");
const li3 = document.createElement("li");

const a1 = document.createElement("a");
const a2 = document.createElement("a");
const a3 = document.createElement("a");

a1.innerText = "Facebook";
a2.innerText = "Instagram";
a3.innerText = "Youtube";

a1.setAttribute("href", "https://www.facebook.com/");
a2.setAttribute("href", "https://www.instagram.com/");
a3.setAttribute("href", "https://www.youtube.com/");

a1.style.color = "red";
a2.style.color = "blue";
a3.style.color = "green";

ul.append(li1, li2, li3);
li1.append(a1);
li2.append(a2);
li3.append(a3);

const contact = document.createElement("p");
contact.innerHTML = "Contact Us";

const lblNama = document.createElement("label");
const lblEmail = document.createElement("label");
const lblPhone = document.createElement("label");

const txNama = document.createElement("input");
const txEmail = document.createElement("input");
const txPhone = document.createElement("input");

lblNama.setAttribute("for", "nama");
lblEmail.setAttribute("for", "email");
lblPhone.setAttribute("for", "phone");

lblNama.innerHTML = "Nama ";
lblEmail.innerHTML = "Email ";
lblPhone.innerHTML = "Phone ";

txNama.setAttribute("type", "text");
txNama.setAttribute("id", "nama");
txEmail.setAttribute("type", "email");
txEmail.setAttribute("id", "email");
txPhone.setAttribute("type", "number");
txPhone.setAttribute("id", "phone");

const br1 = document.createElement("br");
const br2 = document.createElement("br");
const br3 = document.createElement("br");
const br4 = document.createElement("br");

const btnRes = document.createElement("button");
const btnSub = document.createElement("button");

btnRes.innerHTML = "Reset";
btnSub.innerHTML = "Submit";

btnRes.style.padding = "5px";
btnRes.style.borderRadius = "5px";
btnSub.style.borderRadius = "5px";

btnRes.style.marginRight = "5px";
btnSub.style.padding = "5px";

btnRes.style.background = "white";
btnSub.style.background = "#2196F3";
btnSub.style.color = "white";
btnSub.style.border = "none";

btnRes.style.border = "1px solid #acacac";

const forms = document.createElement("form");
forms.append(lblNama, txNama, br1, lblEmail, txEmail, br2, lblPhone, txPhone, br3, br4, btnRes, btnSub);

body.append(text, img, h3, ul, contact, forms);