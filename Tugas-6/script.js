// no 1
const content = document.querySelector(".content");
content.style.backgroundColor = "lightBlue";
content.style.padding = "50px";

// no 2
const h1 = document.querySelector("h1");
h1.innerText = "JUDUL 1";
h1.style.textAlign = "center";

// no 3
const p = document.querySelectorAll("p");
for (let i = 0; i < p.length; i++) {
  p[i].innerText = "isi paragraf";
  p[i].style.color = "#666";
}

// no 4
const list = document.querySelectorAll("ul li a");
for (let i = 0; i < list.length; i++) {
  list[i].innerText = `List ${i + 1}`;
}
list[0].style.color = "red";
list[1].style.color = "yellow";
list[2].style.color = "black";
list[3].style.color = "orange";
list[4].style.color = "#acacac";
