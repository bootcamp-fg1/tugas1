const login = document.querySelector(".btn-login");
const modal = document.querySelector(".modal");
const cancel = document.querySelector(".btn-cancel");

login.addEventListener("click", () => {
  modal.classList.add("active");
});

cancel.addEventListener("click", (e) => {
  e.preventDefault();
  modal.classList.remove("active");
});
